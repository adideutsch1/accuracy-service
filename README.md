# accuracy-service
> Analyse the accuracy of your ML predictive models!

## Table of contents
* [Setup](#setup)
* [Features](#features)
* [Contact](#contact)

## Setup
Run app.py using Python 3.7 with Flask and SQLAlchemy installed. For an easy ongoing usage, use system schedulers such as systemd or cron to run the app automatically for you.

In Ubuntu, Mint and Debian you can install Python 3 like this:

    $ sudo apt-get install python3 python3-pip

For other Linux flavors, macOS and Windows, packages are available at

  http://www.python.org/getit/

To install required packages (Flask and SQLAlchemy) run from project directory:

    $ pip3 install -r requirements.txt 

## Features
* Retrieve from the DB all relevant prediction and actual values.
* Calculate the overall average “accuracy” of the relevant predictions, by comparing the predicted value vs the actual value of each relevant record.

## Contact
Created by [@adideutsch1](adideutsch1@gmail.com) - feel free to contact me!