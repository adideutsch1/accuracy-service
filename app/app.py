from flask import Flask, request, jsonify

import model

app = Flask(__name__)


@app.route('/v1/accuracy/version/<version_id>')
def get_accuracy(version_id):
    segment_id = request.args.get("segment_id")
    return jsonify({"accuracy": model.get_accuracy(version_id, segment_id)})


if __name__ == '__main__':
    app.run(port=8080)
