from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from db_utils import DBSession


Base = declarative_base()


class Predictions(Base):
    """
    ML Predictions
    """
    __tablename__ = 'predictions'
    version_id = Column(Integer)
    segment_id = Column(Integer)
    record_id = Column(Text, primary_key=True)
    prediction_value = Column(Integer)

    def __repr__(self):
        return f"<Prediction(version_id='{self.version_id}', " \
                            f"segment_id='{self.segment_id}', " \
                            f"record_id='{self.record_id}', " \
                            f"prediction_value='{self.prediction_value}')>"


class Actual(Base):
    """
    Actual data (to be predicted)
    """
    __tablename__ = 'actual'
    record_id = Column(Text, primary_key=True)
    actual_value = Column(Integer)

    def __repr__(self):
        return f"<Actual(record_id='{self.record_id}', " \
                       f"actual_value='{self.actual_value}')>"


def get_accuracy(version_id, segment_id):
    """
    Calculate the accuracy of predictions for a give version_id (with an optional segmaent_id)
    """
    session = DBSession().get_db_session()
    join_query = session.query(Predictions, Actual).filter(Predictions.version_id == version_id) \
                                                   .filter(Predictions.record_id == Actual.record_id)
    if segment_id is not None:
        join_query = join_query.filter(Predictions.segment_id == segment_id)

    total_predictions, correct_predictions = 0, 0
    for prediction, actual in join_query.all():
        if prediction.prediction_value == actual.actual_value:
            correct_predictions += 1
        total_predictions += 1
    return (correct_predictions + 0.0) / total_predictions
